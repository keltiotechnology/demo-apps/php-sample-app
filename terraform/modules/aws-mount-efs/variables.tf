# Input variable definitions
variable "vpc_id" {
    type = string
    description = "The VPC ID where we will create mount EFS."
}

variable "subnet_ids" {
    type = list
    description = "The private subnets ID in the VPC where we will create mount EFS."
}

variable "file_name" {
    description = "The file name of generated file e.g. .ebextensions/efs-create.config"
    type = string
    default = ".ebextensions/efs-create.config"
}