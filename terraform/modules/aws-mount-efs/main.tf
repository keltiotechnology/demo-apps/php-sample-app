locals {
  suffix_names = ["A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
  "Z"]

  numberOfSubnets = length(var.subnet_ids)
  subnetMap            = zipmap(slice(local.suffix_names, 0, local.numberOfSubnets), var.subnet_ids)

  efsCreate_generated_template    = templatefile("${path.module}/efs-create.tpl", { data = local.subnetMap, vpc_id = var.vpc_id })
}


# Generate an Elastic Beanstalk configuration file to create EFS mount target
resource "local_file" "mount-efs" {
  content  = local.efsCreate_generated_template
  filename = var.file_name #".ebextensions/efs-create.config"

}
