This Module is used to generate a configuration file placed in the .ebextension folder. For Beanstalk application, the generated file will be run after you deploy bundled source code to Beanstalk environment.

The module is based on [this](https://github.com/awsdocs/elastic-beanstalk-samples/blob/main/configuration-files/aws-provided/instance-configuration/storage-efs-createfilesystem.config) sample released by AWS

The purpose of genereated file is to create Mount Targets in each available zone in a VPC so that EC instances can interact with the file system.

Here we need to provide the following input value:

- vpc_id: The VPC ID
- subnet_ids: A list of Private subnet ids


Note:
- This module allows to add up to 26 subnets as input. We actually can only create a mount target at most one per Availability zone. 
- AWS sample configuration file mentions that in order to have all instances can connect to the file system, we MUST have mount target for default each subnet. Usually we will mount file system in private subnets.
- AWS docs recommends us to have access the file system from a mount target within the same Availability Zone for performance and cost reasons. If there is more than one subnet in the Availability Zone, we only need have one mount target created in ONLY of the subnets.
- More details can be found [here](https://docs.aws.amazon.com/efs/latest/ug/manage-fs-access.html) 


Usage:

```
module "aws-mount-efs" {
  source     = "./modules/aws-mount-efs"
  vpc_id     = <Your VPC ID>
  subnet_ids = <Your Subnet Ids>
  file_name  = "../.ebextensions/efs-create.config"
}
```