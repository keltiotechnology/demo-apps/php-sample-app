###############################################################################
## Find your default VPC in the console, and fill in the VPC ID and subnet IDs
## below. If you have more or less than three AZs and subnets in your region,
## adjust the number of Subnet Option  and Mount Target Resource entries to 
## match.
###############################################################################

option_settings:
  aws:elasticbeanstalk:customoption:
    EFSVolumeName: "EB-EFS-Volume"
    VPCId: "${vpc_id}"
## Subnet Options
%{ for key, value in data ~}    
    Subnet${key}: "${value}"
%{ endfor ~}

Resources:
## Mount Target Resources
%{ for key, value in data ~}
  MountTarget${key}:
    Type: AWS::EFS::MountTarget
    Properties:
      FileSystemId: {Ref: FileSystem}
      SecurityGroups:
        - {Ref: MountTargetSecurityGroup}
      SubnetId:
        Fn::GetOptionSetting: {OptionName: Subnet${key}}
%{ endfor ~}

##############################################
#### Do not modify values below this line ####
##############################################

  FileSystem:
    Type: AWS::EFS::FileSystem
    Properties:
      FileSystemTags:
      - Key: Name
        Value:
          Fn::GetOptionSetting: {OptionName: EFSVolumeName, DefaultValue: "EB_EFS_Volume"}

  MountTargetSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Security group for mount target
      SecurityGroupIngress:
      - FromPort: '2049'
        IpProtocol: tcp
        SourceSecurityGroupId:
          Fn::GetAtt: [AWSEBSecurityGroup, GroupId]
        ToPort: '2049'
      VpcId:
        Fn::GetOptionSetting: {OptionName: VPCId}
