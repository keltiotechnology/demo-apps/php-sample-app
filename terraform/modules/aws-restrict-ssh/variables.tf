# Input variable definitions
variable "ssh_restricted_ip_address" {
    description = "The IP addresss or IP address range allowed to gain SSH access to EC instances"
    type = string
}

variable "file_name" {
    description = "The file name of generated file e.g .ebextensions/dev.config"
    type = string
    default = ".ebextensions/dev.config"
}