locals {
  sshRestrict_generated_template = templatefile("${path.module}/dev_config.tpl", { ssh_restricted_ip_address = var.ssh_restricted_ip_address })
}

# Generate an Elastic Beanstalk configuration file to restrict SSH access from only specify IP address (range)
resource "local_file" "restrict_ssh" {
  content  = local.sshRestrict_generated_template
  filename = var.file_name #".ebextensions/dev.config"
}
