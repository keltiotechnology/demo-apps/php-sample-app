This Module is used to generate a configuration file placed in the .ebextension folder. For Beanstalk application, the generated file will be run after you deploy bundled source code to Beanstalk environment.

The module is based on [this](https://github.com/awsdocs/elastic-beanstalk-samples/blob/main/configuration-files/aws-provided/instance-configuration/storage-efs-createfilesystem.config) sample released by AWS

The purpose of genereated file is restrict SSH access to EC2 instance in the Beanstalk enviroment. This is just to we can securely install Wordpress. After the installation complete, we can remove the generated dev.config file from the the bundled source code. 

Usage
```
module "aws-restrict-ssh" {
  source                    = "./modules/aws-restrict-ssh"
  ssh_restricted_ip_address = <Your IP Address/IP Address range>
  file_name                 = "../.ebextensions/dev.config"
}
```