# Refer to this file as example: 
# https://github.com/cloudposse/terraform-aws-elastic-beanstalk-environment/blob/master/examples/complete/fixtures.us-east-2.tfvars

region = "eu-central-1"

availability_zones = ["eu-central-1a", "eu-central-1b"]

namespace = "peter"

stage = "test"

name = "wordpress"

description = "eb-env"

tier = "WebServer"

environment_type = "LoadBalanced"

loadbalancer_type = "application"

availability_zone_selector = "Any 2"

instance_type = "t2.micro"

autoscale_min = 1

autoscale_max = 2

wait_for_ready_timeout = "20m"

force_destroy = true

rolling_update_enabled = true

rolling_update_type = "Health"

updating_min_in_service = 0

updating_max_batch = 1

healthcheck_url = "/"

application_port = 80

root_volume_size = 30

root_volume_type = "gp2"

autoscale_measure_name = "CPUUtilization"

autoscale_statistic = "Average"

autoscale_unit = "Percent"

autoscale_lower_bound = 20

autoscale_lower_increment = -1

autoscale_upper_bound = 80

autoscale_upper_increment = 1

elb_scheme = "public"

// https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html
// https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html#platforms-supported.PHP
solution_stack_name = "64bit Amazon Linux 2 v3.3.1 running PHP 7.4"

version_label = "1.0"

dns_zone_id = "Z02042992EWARW63784GW"

// https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html
additional_settings = [
  {
    namespace = "aws:elasticbeanstalk:environment:process:default"
    name      = "StickinessEnabled"
    value     = "false"
  },
  {
    namespace = "aws:elasticbeanstalk:managedactions"
    name      = "ManagedActionsEnabled"
    value     = "false"
  }
]

env_vars = {
  "RDS_DB_NAME"      = "eldb"
  "RDS_HOSTNAME"     = "wp-db"
  "RDS_USERNAME"     = "admin"
  "RDS_PASSWORD"     = "dbtest123321"
  "RDS_PORT"         = "3306" 
}


// https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/environments-cfg-autoscaling-scheduledactions.html
scheduled_actions = [
  {
    name            = "Refreshinstances"
    minsize         = "1"
    maxsize         = "2"
    desiredcapacity = "2"
    starttime       = "2021-06-11T07:00:00Z"
    endtime         = "2016-06-12T07:00:00Z"
    recurrence      = "*/20 * * * *"
    suspend         = false
  }
]

ssh_restricted_ip_address = "42.113.164.109/32"