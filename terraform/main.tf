# Refer to this file as example: 
# https://github.com/cloudposse/terraform-aws-elastic-beanstalk-environment/blob/master/examples/complete/main.tf

provider "aws" {
  region = var.region
}

/* provider "tfe" {
  # Configuration options
  
} */

module "vpc" {
  source     = "cloudposse/vpc/aws"
  version    = "0.25.0"
  cidr_block = "10.0.0.0/16"
  name       = "${var.namespace}/${var.stage}/${var.name} VPC"

  context = module.this.context
}

module "subnets" {
  source               = "cloudposse/dynamic-subnets/aws"
  version              = "0.39.3"
  availability_zones   = var.availability_zones
  vpc_id               = module.vpc.vpc_id
  igw_id               = module.vpc.igw_id
  cidr_block           = module.vpc.vpc_cidr_block
  nat_gateway_enabled  = true
  nat_instance_enabled = false

  context = module.this.context
}

data "aws_iam_policy_document" "minimal_s3_permissions" {
  statement {
    sid = "AllowS3OperationsOnElasticBeanstalkBuckets"
    actions = [
      "s3:ListAllMyBuckets",
      "s3:GetBucketLocation"
    ]
    resources = ["*"]
  }
}

# Local Module to mount EFS
module "aws-mount-efs" {
  source     = "./modules/aws-mount-efs"
  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.subnets.private_subnet_ids
  file_name  = "../.ebextensions/efs-create.config"
}

# Local Module to restricting SSH access
module "aws-restrict-ssh" {
  source                    = "./modules/aws-restrict-ssh"
  ssh_restricted_ip_address = var.ssh_restricted_ip_address
  file_name                 = "../.ebextensions/dev.config"
}

module "rds_instance" {
  source                      = "cloudposse/rds/aws"
  version                     = "0.35.1"
  namespace                   = var.namespace
  environment                 = "rds"
  stage                       = var.stage
  name                        = var.name
  dns_zone_id                 = var.dns_zone_id
  host_name                   = var.env_vars.RDS_HOSTNAME
  security_group_ids          = [module.vpc.vpc_default_security_group_id]
  ca_cert_identifier          = "rds-ca-2019"
  allowed_cidr_blocks         = [module.vpc.vpc_cidr_block]
  database_name               = var.env_vars["RDS_DB_NAME"]
  database_user               = var.env_vars["RDS_USERNAME"]
  database_password           = var.env_vars["RDS_PASSWORD"]
  database_port               = var.env_vars["RDS_PORT"]
  multi_az                    = true
  storage_type                = "gp2"
  allocated_storage           = 100
  storage_encrypted           = true
  engine                      = "mysql"
  engine_version              = "5.7.17"
  major_engine_version        = "5.7"
  instance_class              = "db.t2.medium"
  db_parameter_group          = "mysql5.7"
  publicly_accessible         = false
  subnet_ids                  = module.subnets.private_subnet_ids
  vpc_id                      = module.vpc.vpc_id
  auto_minor_version_upgrade  = true
  allow_major_version_upgrade = false
  apply_immediately           = false
  maintenance_window          = "Mon:03:00-Mon:04:00"
  skip_final_snapshot         = false
  copy_tags_to_snapshot       = true
  backup_retention_period     = 7
  backup_window               = "22:00-03:00"
}



module "elastic_beanstalk_application" {
  source      = "cloudposse/elastic-beanstalk-application/aws"
  version     = "0.8.0"
  description = "${var.namespace}/${var.stage}/${var.name} application"

  context = module.this.context
}

module "elastic_beanstalk_environment" {
  version                            = "0.40.0"
  source                             = "cloudposse/elastic-beanstalk-environment/aws"
  description                        = var.description
  region                             = var.region
  availability_zone_selector         = var.availability_zone_selector
  dns_zone_id                        = var.dns_zone_id
  environment                        = "EB"
  wait_for_ready_timeout             = var.wait_for_ready_timeout
  elastic_beanstalk_application_name = module.elastic_beanstalk_application.elastic_beanstalk_application_name
  environment_type                   = var.environment_type
  loadbalancer_type                  = var.loadbalancer_type
  elb_scheme                         = var.elb_scheme
  tier                               = var.tier
  version_label                      = aws_elastic_beanstalk_application_version.default.name #var.version_label
  force_destroy                      = var.force_destroy

  instance_type    = var.instance_type
  root_volume_size = var.root_volume_size
  root_volume_type = var.root_volume_type

  autoscale_min             = var.autoscale_min
  autoscale_max             = var.autoscale_max
  autoscale_measure_name    = var.autoscale_measure_name
  autoscale_statistic       = var.autoscale_statistic
  autoscale_unit            = var.autoscale_unit
  autoscale_lower_bound     = var.autoscale_lower_bound
  autoscale_lower_increment = var.autoscale_lower_increment
  autoscale_upper_bound     = var.autoscale_upper_bound
  autoscale_upper_increment = var.autoscale_upper_increment

  vpc_id                  = module.vpc.vpc_id
  loadbalancer_subnets    = module.subnets.public_subnet_ids
  application_subnets     = module.subnets.private_subnet_ids
  allowed_security_groups = [] #[module.vpc.vpc_default_security_group_id]

  rolling_update_enabled  = var.rolling_update_enabled
  rolling_update_type     = var.rolling_update_type
  updating_min_in_service = var.updating_min_in_service
  updating_max_batch      = var.updating_max_batch

  healthcheck_url  = var.healthcheck_url
  application_port = var.application_port

  # https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html
  # https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html#platforms-supported.docker
  solution_stack_name = var.solution_stack_name
  additional_settings = var.additional_settings
  env_vars                     = merge(var.env_vars, { "RDS_HOSTNAME" : module.rds_instance.hostname })
  extended_ec2_policy_document = data.aws_iam_policy_document.minimal_s3_permissions.json
  prefer_legacy_ssm_policy     = false
  prefer_legacy_service_policy = false
  context                      = module.this.context
  depends_on                   = [module.rds_instance.id]
}

resource "aws_s3_bucket" "beanstalk" {
  bucket = join("-", ["${var.name}" ,substr(sha1("${var.name}"),0,10)])
  tags = {
    Name        = "${var.name}"
    Environment =  "${var.stage}"
  }
}

resource "aws_s3_bucket_object" "application" {
  bucket =  aws_s3_bucket.beanstalk.id
  key    = "beanstalk/${var.name}-v${var.version_label}.zip"
  source = "bundles/${var.name}-v${var.version_label}.zip"

  depends_on = [
    null_resource.bundle_source_code
  ]
}

# Run bash command to bundle application source for deployment
resource "null_resource" "bundle_source_code" {
  provisioner "local-exec" {
    command = "cd .. && zip terraform/bundles/${var.name}-v${var.version_label}.zip -r * .[^.]* -x 'terraform/*' -x '.terraform/*' -x '.git/*' "
  }
  depends_on = [
    aws_s3_bucket.beanstalk
  ]
}



resource "aws_elastic_beanstalk_application_version" "default" {
  name        = "${var.name}_v${var.version_label}"
  application = module.elastic_beanstalk_application.elastic_beanstalk_application_name
  description = "application version created by terraform"
  bucket      = aws_s3_bucket.beanstalk.id
  key         = aws_s3_bucket_object.application.id
}