FROM hashicorp/terraform:light

COPY . /usr/src/wordpress
WORKDIR /usr/src/wordpress/terraform
